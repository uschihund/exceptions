package com.company;

import org.junit.Test;

import static org.junit.Assert.*;

public class AccountTest
{
    @Test
    public void withdraw_should_throw_InsufficientBalanceException_on_insufficient_balance() throws Exception
    {
        Account account = new Account();

        account.withdraw(10);

        //modify to test for the exception
    }

    @Test
    public void withdraw_should_reduce_balance_by_amount()
    {
        //add code here
    }

    @Test
    public void deposit_should_increase_balance_by_amount()
    {
        //add code here
    }

    @Test
    public void new_account_should_start_with_balance_0()
    {
        Account account = new Account();

        int result = account.getBalance();

        assertEquals(0, result);
    }
}
