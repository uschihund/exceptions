package com.company;

import org.junit.Test;

import static org.junit.Assert.*;

public class ExamplesTest
{
    @Test
    public void stringLength_should_return_0_on_null_input() throws Exception
    {
        Examples examples = new Examples();

        examples.stringLength(null);
    }

    public void everyTimeNullPointerException_should_always_throw_exception()
    {
        Examples examples = new Examples();

        boolean nullPointerExceptionCatched = false;

        examples.everyTimeNullPointerException();
        //modify code here

        assertTrue(nullPointerExceptionCatched);
    }

    @Test(expected = NullPointerException.class)
    public void everyTimeNullPointerException_should_always_throw_exception_with_expected()
    {
        Examples examples = new Examples();

        examples.everyTimeNullPointerException();
    }
}
